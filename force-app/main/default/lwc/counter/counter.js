import { LightningElement } from 'lwc';

export default class Counter extends LightningElement {

    counter= 0;
    handlerIncrement(){
        this.counter++;
    }

    handlerDecrement(){
        this.counter--;
    }
}