import { LightningElement, track, wire} from 'lwc';
import getCars from '@salesforce/apex/CarsClass.getCars';
import carIcon from '@salesforce/resourceUrl/carIcon';
import searchCars from '@salesforce/apex/CarsController.searchCars';
import getAllCars from '@salesforce/apex/CarsController.getAllCars';

export default class CarscardList extends LightningElement {

    @track Cars_c;
    CarsPage;
    error;
    car=carIcon;
    key;

    ssearchTerm = '';
	@wire(searchCars, {searchTerm: '$searchTerm'})
	cars;

    handleSearchTermChange(event) {
		// Debouncing this method: do not update the reactive property as
		// long as this function is being called within a delay of 300 ms.
		// This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchTerm = event.target.value;
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.searchTerm = searchTerm;
		}, 300);
	}

    

    handleSearch(){
        //call the apex method
        getAllCars({searchKey: this.key})
        .then(result=>{
            this.Cars_c=result;
        })
        .catch(error=> {
            this.Cars_c= null;
        });
    }

    colms=[
        {label:'Brand Name', fieldName:'Brand_Name__c', type:'text' },
        {label:'Country origin', fieldName:'Country_origin__c', type:'text' },
        {label:'License Plate', fieldName:' License_Plate__c', type:'text' }
    ]


    connectedCallback(){
        getCars()
        .then(result=>{
            this.CarsPage=result;
            console.log("this.CarsPage:"+ JSON.stringfy(this.CarsPage));
        })

        .catch(error=>{
            this.error=error;
        })
    }
    handleCarView(event) {
		// Get bear record id from bearview event
		const carsId = event.detail;
		// Navigate to bear record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: c00B7Q00000RUwvZUAT,
				objectApiName: 'Cars__c',
				actionName: 'view',
			},
		});
	}
}