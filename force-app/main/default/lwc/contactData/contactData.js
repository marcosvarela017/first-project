import { LightningElement, wire, api, track } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Contact.Name';
import BIRTHDATE_FIELD from '@salesforce/schema/Contact.Birthdate';
import OTHERCROUNTRY_FIELD from '@salesforce/schema/Contact.OtherCountry';
import getContacts from '@salesforce/apex/TheContact.getContacts'


const COLUMNS= [
    {label: 'Name', fieldName: NAME_FIELD.fieldApiName, type:'text'},
    {label: 'Birthdate', fieldName: BIRTHDATE_FIELD.fieldApiName, type:'date'},
    {label: 'Country', fieldName: OTHERCROUNTRY_FIELD.fieldApiName, type:'text'},

];

export default class ContactData extends LightningElement {
    columns=COLUMNS;
    @wire(getContacts)contacts;
}