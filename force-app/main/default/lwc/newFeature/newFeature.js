import { LightningElement } from 'lwc';
export default class HelloWorld extends LightningElement {
  greeting = 'Marcos';
  changeHandler(event) {
    this.greeting = event.target.value;
  }
}