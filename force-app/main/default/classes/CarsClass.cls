public class CarsClass {
	@AuraEnabled
    Public static List<Cars__c> getCars(){
    	List<Cars__c> allCars = new List <Cars__c>([SELECT Id, License_Plate__c, VehicleIdentificationNumber__c, Name, Brand_Name__c, 
                                                     Cars_Shop_Name__r.Name, Country_origin__c, Model__c FROM Cars__c]);
        if (!allCars.isEmpty()){
           Return allCars; 
        }
        
         Return Null;
    }   
    
   
}