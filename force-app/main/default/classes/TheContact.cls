public with sharing class TheContact {

    @AuraEnabled(cacheable=true)
    public static List<Contact> getContacts(){
        return[
            SELECT Name, Birthdate, Othercountry 
            FROM Contact
        ];
    }
}