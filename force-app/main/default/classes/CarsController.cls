public with sharing class CarsController {
    @AuraEnabled(cacheable=true scope='global')
    public static Cars__c[] getAllCars() {
        return [
            SELECT
            VehicleIdentificationNumber__c, License_Plate__c, Name, Brand_Name__c, 
            Cars_Shop_Name__r.Name, Country_origin__c, Model__c
            FROM Cars__c
            WITH SECURITY_ENFORCED
            ORDER BY Name
            LIMIT 50
        ];
    }

    @AuraEnabled(cacheable=true scope='global')
    public static Cars__c[] searchCars(String searchTerm) {
        // Return all cars when no search term
        searchTerm = searchTerm.trim();
        if (searchTerm == '') {
            return getAllCars();
        }
        // Prepare query paramters
        searchTerm = '%' + searchTerm + '%';
        // Execute search query
        return [
            SELECT
            License_Plate__c, VehicleIdentificationNumber__c, Name, Brand_Name__c, 
            Cars_Shop_Name__r.Name, Country_origin__c, Model__c
            FROM Cars__c
            WHERE Name LIKE :searchTerm
            WITH SECURITY_ENFORCED
            ORDER BY Name
            LIMIT 50
        ];
    }
}