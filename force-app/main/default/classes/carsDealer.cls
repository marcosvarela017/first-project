public class carsDealer {
    public static void carbrand(Integer value, Integer traveledDistance, Integer carYear, String brand, String island, id carshop){ 
        Integer carsCounter= 1;
        
        List <Cars__c> totalCars = new List <Cars__c>();
        List<String> carModel = new List<String>{'Benz','Cayene','Corrola','Chiron', 'Diablo'};   
        Integer automaticNum= Integer.valueOf(Math.Random() * 1000);   
        DateTime myDateTime = DateTime.now();
        
        while (carsCounter <= value){
            System.debug('Yes, now we are available to put some cars into our list'+ carsCounter);
            
            //Instance our Object
            Cars__c populate = new Cars__c();
            
            //Saving values into our attribute variables name
            populate.Name= myDateTime +'.'+ myDateTime.millisecond() + carsCounter +  ' Car';
            populate.Color__c=CarsAttribute.rColors(); 
            populate.Brand_Name__c= brand == Null ? Brd() : brand;
            populate.Model__c= Models(brand);
            populate.VehicleIdentificationNumber__c= CarsAttribute.vin();
            populate.License_Plate__c= CarsAttribute.licencePlate(island);
            populate.Travelled_Distance__c= traveledDistance;
            populate.Cars_Years__c= carYear;
            populate.Cars_Shop_Name__c= carshop;
            string bd= populate.Brand_Name__c;
            populate.Car_Price__c = carsPrice(bd);
            
            
            //Create traveledDistance variable to store Cars distance Separating Cars from the Factory from used Cars    
            if (traveledDistance > 0) { 
                populate.RecordTypeId= Schema.SObjectType.Cars__c.getRecordTypeInfosByDeveloperName().get('CarsInSecondHand').getRecordTypeId();     
            } else{
                populate.RecordTypeId= Schema.SObjectType.Cars__c.getRecordTypeInfosByDeveloperName().get('FromTheFactory').getRecordTypeId();
            }
            
            //increment the carsCounter 
            totalCars.add(populate);
            System.debug('Our Cars counter' + carsCounter);
            carsCounter ++;
        } 
        
        
        System.debug('Size og Car List' + totalCars.size());
        System.debug('Elementsin Car List: ' + totalCars);
        
        //Insert data (cars), in our Org
        insert totalCars; 
        
    }
    
    //----------------------------------------Delete cars Method from our Totalcars list---------------------------------------------------------
    
    public static void DeletCars(){
        List<Cars__c> totalCars = [SELECT Id, Name FROM Cars__c WHERE Cars_Years__c >= 2000]; 
        delete totalCars;
    }
    
    //----------------------------------------Update carsMethod from our Totalcars list---------------------------------------------------------
    
    public static void UpdateCars(string brand){
        List<Cars__c> carsUpdate = [SELECT Id, Name,Brand_Name__c FROM Cars__c WHERE Brand_Name__c =:brand AND Country_origin__c = NULL];
        List<Cars__c> newList = new List <Cars__c> ();
        
        for (Cars__c UpdateCars :carsUpdate){
            system.debug(UpdateCars);
            switch on brand {
                
                when 'Mazda', 'Mitsubishi', 'Toyota', 'Subaru', 'Nissan', 'Honda'{
                    UpdateCars.Country_origin__c = 'JAPAN';
                    newList.add(UpdateCars);
                }
                
                When 'Cadillac', 'Dodge', 'Ford ', 'Jeep', 'General Motors(GMC)', 'Buick', 'Sterling', 'Chevrolet'{
                    UpdateCars.Country_origin__c = 'USA';
                    newList.add(UpdateCars);
                }
                
                When 'BMW', 'Mercedes Benz', 'Porsche', 'Ford-Werke GmbH', 'Auto union', 'Volkswagen‎ ','Audi'{
                    UpdateCars.Country_origin__c = 'GERMANY';
                    newList.add(UpdateCars);
                }
                
                When 'Mazzanti', 'Alfa Romeo ', 'Ferrari', 'Lamborghini', 'Lancia', 'Maserati', 'Pagani', 'Fiat '{
                    UpdateCars.Country_origin__c = 'ITALY';
                    newList.add(UpdateCars);
                }
                
                When 'Pegeout', 'Renault', 'Citroen / DS', 'Alpine', 'Venturi', 'Axiam', 'De La Chapelle', 'Bugatti'{
                    UpdateCars.Country_origin__c = 'FRANCE';
                    newList.add(UpdateCars);
                } 
                
                When else{
                    system.debug ('Give us more details to know this car origin ');
                    
                } 
            }
            
        }
        update newList;
        
        
    }
    
    //----------------------------------------Update cars model from our carBrand list---------------------------------------------------------
    
    public static String Models(String brand){
        List <String> listBrandAudi = new List<String>{'A7', 'A6', 'A3'};
            List <String> listToyotaBrand = new List<String>{'Sienna', 'Tundra', 'Highlander', 'Corolla Cross', 'Land Cruiser'};
                List <String> listMazdaBrand = new List<String>{'Homura', 'Prime-Line', 'Exclusive-line', 'CX 30', '626'};
                    List <String> listJeepBrand = new List<String>{'Avenger', 'Bolivia', 'Renegade', 'Wrangler', 'Gladiator'};
                        List <String> listBugattiBrand = new List<String>{'Veyron', 'Chiron', 'Divo', 'Centodieci', 'Bolide', 'Mistral'};
                            List <String> listAlfaBrand = new List<String>{'Guilia luxury sedan', 'Stelvio luxury SUV', '', 'QUADRIFOGLIO'};
                                List <String> listChevBrand = new List<String>{'TRAILBLAZER', 'Camaro', 'EQUINOX', 'BOLT EV', 'MALIBU'};
                                    List <String> listHondaBrand = new List<String>{'Pilot', 'Fit', 'H RV', 'Civic', 'Jazz'};
                                        List <String> listBMWBrand = new List<String>{'X2', 'X3', 'X5', 'X6', 'X1', 'X7', ' iX M60', 'X7 M60i'};
                                            List <String> listSubaruBrand = new List<String>{'XV e-BOXER', 'Forester e-BOXER', 'Impreza', 'WRX'};
                                                List <String> listlamboBrand = new List<String>{'AVENTADOR LP 780-4 ULTIMAE', 'AVENTADOR LP 780-4 ULTIMAE ROADSTER', 'AVENTADOR SVJ', 
                                                    'AVENTADOR SVJ ROADSTER', 'URUS S', 'URUS PEARL CAPSULE', 'SIÁN FKP 37', 'SIÁN ROADSTER'};
                                                        List <String> listventuriBrand = new List<String>{'Atlantique 300 Biturbo', '300 GTR', 'Volage', 'Astrolab', '600 SLM'};    
                                                            
        String model;
        switch on brand{
            when 'Audi' {
                Integer index = Integer.valueof((Math.random() * listBrandAudi.size()));
                model= listBrandAudi[index];
                
            } when 'Mazda' {
                Integer index = Integer.valueof((Math.random() * listMazdaBrand.size()));
                model= listMazdaBrand[index];
                
            } when 'Jeep' {
                Integer index = Integer.valueof((Math.random() * listJeepBrand.size()));
                model= listJeepBrand[index];
                
            } when 'Lamborghini' {
                Integer index = Integer.valueof((Math.random() * listlamboBrand.size()));
                model= listlamboBrand [index];
                
            }when 'Honda' {
                Integer index = Integer.valueof((Math.random() * listHondaBrand.size()));
                model= listHondaBrand[index];
                
            }  when 'Venturi' {
                Integer index = Integer.valueof((Math.random() * listventuriBrand.size()));
                model= listventuriBrand [index];
            } 
            
            when 'Alfa Romeo' {
                Integer index = Integer.valueof((Math.random() * listAlfaBrand.size()));
                model= listAlfaBrand [index];
            }
            
            when 'BMW' {
                Integer index = Integer.valueof((Math.random() * listBMWBrand.size()));
                model= listBMWBrand [index];
            }
            
            when 'Subaru' {
                Integer index = Integer.valueof((Math.random() * listSubaruBrand.size()));
                model= listSubaruBrand [index];
            }
            
            when 'Chevrolet' {
                Integer index = Integer.valueof((Math.random() * listChevBrand.size()));
                model= listChevBrand [index];
            }
            
            when 'Bugatti' {
                Integer index = Integer.valueof((Math.random() * listBugattiBrand.size()));
                model= listBugattiBrand [index];
            }
            
            
            
            when else{
                System.debug('Brand not found');
            }
            
        } return model;       
    }
    
    //---------------------------------------------------------------RandomPickListValues--------------------------------------------------
    
    public static String Brd(){
        Schema.DescribeFieldResult fieldResult = Car_Sales_Motor_Shop__c.Specific_Brand_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<String> brands = new List<String>();
        String anyBrand ='';
        for(Schema.PicklistEntry s:ple){ 
            brands.add(s.getLabel());   
        }
        
        Integer brandsSize = brands.size();
        Integer randomBrands = Integer.valueof((Math.random() * brandsSize));
        String  randomBr= brands[randomBrands];
        
        Return randomBr;
    }
    
    Public Static Decimal carsPrice(string brands){       
        //List<Cars__c> carsValue = [SELECT Id, Name, Brand_Name__c FROM Cars__c WHERE Brand_Name__c =:brands ];
        List<Cars__c> carsValue = [SELECT Id, Name, Brand_Name__c FROM Cars__c ];
        Decimal prices;
        System.debug('Honda' + prices);
        for (Cars__c price :carsValue){
            switch on brands{
                when 'Mazda'{
                    price.Car_Price__c= 65000;
                    prices= price.Car_Price__c;
                    
                }
                when 'Toyota'{
                    price.Car_Price__c= 10000;
                    prices= price.Car_Price__c;
                }
                when 'Jeep'{
                    price.Car_Price__c= 70000;
                    prices= price.Car_Price__c;
                } 
                when 'BMW'{
                    price.Car_Price__c= 20000;
                    prices= price.Car_Price__c;
                }
                when 'Alfa Romeo'{
                    price.Car_Price__c= 70000;
                    prices= price.Car_Price__c;
                }
                when 'Subaru'{
                    price.Car_Price__c= 71000;
                    prices= price.Car_Price__c;
                }
                when 'Bugatti'{
                    price.Car_Price__c= 71000;
                    prices= price.Car_Price__c;
                }
                when 'Venturi'{
                    price.Car_Price__c= 82000;
                    prices= price.Car_Price__c;
                }
                when 'Lamborghini'{
                    price.Car_Price__c= 82000;
                    prices= price.Car_Price__c;
                }
                when 'Chevrolet'{
                    price.Car_Price__c= 82000;
                    prices= price.Car_Price__c;
                }
                when 'Audi'{
                    price.Car_Price__c= 60000;
                    prices= price.Car_Price__c;
                }
                when 'Honda'{
                    price.Car_Price__c= 60000;
                    prices= price.Car_Price__c;
                    System.debug('Honda' + prices);
                }
                
            }
        } 
        System.debug('Honda' + prices);
        return prices;
    }
    
    //---------------------------------------------------------------Decrement Cars in Shop------------------------------------------------
    public static void carCount(string idShop){
      AggregateResult[] carsCount=[SELECT Count(Name) FROM  Cars__c WHERE Cars_Shop_Name__c =:idShop];
      Car_Sales_Motor_Shop__c cars =[SELECT Name FROM Car_Sales_Motor_Shop__c WHERE id =:idShop];
      Integer count;
      for(AggregateResult cr: carsCount){
      	count=(Integer)cr.get('expr0');
        cars.Number_of_Cars_to_Create__c =count;   
      }
        Update cars;
    }
    
    //---------------------------------------------------------------Geolocation-----------------------------------------------------------
    
    public static void updateLocation(string idShop, string Islnd){
        Car_Sales_Motor_Shop__c shop= [SELECT id FROM Car_Sales_Motor_Shop__c WHERE id =: idShop];
        
        Map<String, Double> mapLatitude=
            new Map<String, Double>{'Santiago'=> 14.916577, 'Assomada'=> 15.096918, 'Tarrafal'=> 15.276881, 'Boa Vista'=> 14.916577, 'São Vicente'=> 16.884769, 'São Nicolau'=> 16.615906,
                'Santo Antão' => 17.019169, 'Fogo'=> 14.894968, 'Santa Maria'=> 16.598580, 'Maio'=> 15.139296, 'Brava'=> 14.871096};
                    
                    Map<String, Double> mapLongitude=
                    new Map<String, Double>{'Santiago'=> -23.509414, 'Assomada'=> -23.666429, 'Tarrafal'=> -23.752130, 'Boa Vista'=> -22.916210, 'São Vicente' => -24.987739, 'São Nicolau' => -24.97,
                        'Santo Antão'=> -25.065610, 'Fogo'=> -24.497447, 'SaL'=> -22.905754, 'Maio' => -23.211581, 'Brava'=> -24.696974};
                            
                            
                            Double latitude;
        for(String keyLatitude : mapLatitude.keySet()){
            if(keylatitude == Islnd){
                latitude = mapLatitude.get(keylatitude);
            }
        }
        
        Double longitude;
        for(String keyLongitude : mapLongitude.keySet()){
            if(keyLongitude == Islnd){
                longitude = mapLongitude.get(keylongitude);
            }
        }
        
        shop.Location__Latitude__s= latitude;
        shop.Location__Longitude__s= longitude;
        update shop;
        
    } 
    
    //----------------------------------------------------------CarsValue function-----------------------------------------------------------
    
    public static void carsValue(string idShop){
        AggregateResult[] carsValue=[SELECT SUM(Car_Price__c) FROM Cars__c WHERE Cars_Shop_Name__c =:idShop];
        Car_Sales_Motor_Shop__c shop =[SELECT Name FROM Car_Sales_Motor_Shop__c WHERE id =:idShop];
        Decimal sum;
        
        for(AggregateResult var :carsValue){
            sum= (Decimal)var.get('expr0');
            shop.Total_Cars_Value__c =sum;
        }
        System.debug('lll'+sum);
        Update shop;
        
    }
    
    
    
    
}