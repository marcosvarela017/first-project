//Create CarsAtribute class reducing our codes and keeping the good practices!
public class CarsAttribute {
    
    //------------------------------------------------------Random Letters----------------------------------------------------------------------- 
    public static String rLetters(Integer length){              
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        String randStr = '';
        while (randStr.length() < length) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);}
        Return randStr;
    }
    
    
    //------------------------------------------------------  Random Color ----------------------------------------------------------------------- 
    
    public static String rColors(){              
        List<String> colors = new List<String>{'Red','Green','Blue','White','Black', 'Violet', 'Magenta', 'Rose'};
            String color = '';
        Integer listSize = colors.size() - 1;
        Integer randomNumber = Integer.valueof((Math.random() * listSize));
        color = colors[randomNumber];
        System.debug(color);
        return color;
        
    }
    
    
    //------------------------------------------------------RandomNumbers----------------------------------------------------------------------- 
    public static String automaticNumbers(Integer length){
        String numbers = '1234567890';
        String randNum = '';
        while (randNum.length() < length) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), numbers.length());
            randNum += numbers.substring(idx, idx+1);
        }
        return randNum;
    }
    
    
    //------------------------------------------------------RandomlicensePlate-----------------------------------------------------------------------
    public static String licencePlate(String island){
        
        Map<String, String> islandCapeVerd = new Map<String,String>();
        islandCapeVerd.put('Santo Antão', 'SA');
        islandCapeVerd.put('São Vicente', 'SV');
        islandCapeVerd.put('São Nicolau', 'SN'); 
        islandCapeVerd.put('Sal', 'SL');
        islandCapeVerd.put('Boa Vista', 'BV'); 
        islandCapeVerd.put('Maio', 'MA'); 
        islandCapeVerd.put('Santiago', 'ST');
        islandCapeVerd.put('Fogo', 'FG');
        islandCapeVerd.put('Brava', 'BR');
        
        String islands= islandCapeVerd.get(island) + '-' + automaticNumbers(2)+'-'+rLetters(2);
        
        return islands;    
    }
    
    public static String vin(){
        String vins = automaticNumbers(1)+rLetters(4)+automaticNumbers(2)+rLetters(4)+automaticNumbers(6);
        return vins;
    }
    
}