trigger RealPotencialV on Opportunity (after insert, after update, after delete) { 
   
    Double realValues = 0;
    Double potencialValue =0;
    String newaccountId;
    String accountId;
	List<Opportunity> getOpp = new List<Opportunity>([SELECT id, StageName, amount FROM Opportunity WHERE AccountID = :accountId]);    
    
    for (Opportunity allopp :Trigger.new){
        String accountId = allopp.AccountId;
        String newaccountId = accountId;
         
  		for (Opportunity sumAmount:getOpp){
            if (sumAmount.StageName == 'Closed-Won'){  
            		realValues = sumAmount.Amount + 1;
                
            } else {
                	potencialValue += sumAmount.Amount;
            }
       }
     
	}
    
    Account newAccount = [SELECT Real_Value__c, Potentials_Values__c  FROM Account WHERE id = :newaccountId];
    newaccount.Real_Value__c= realValues;
    newaccount.Potentials_Values__c= potencialValue;
    
    update newAccount;
}    
    

 /*
    List<Decimal>sumAmount= new List<Decimal> ([SELECT SUM (amount) FROM Opportunity]);
    //Decimal B;
    //B = [SELECT SUM (amount) FROM Opportunity];
    List<Account> allopp = new List<Account>(
        [SELECT Id,(SELECT Id FROM Opportunities) FROM Account]);
    */