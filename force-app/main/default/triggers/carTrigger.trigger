trigger carTrigger on Cars__c (after insert, after update, after Delete) {
    
    if (Trigger.isInsert){
        for(Cars__c auto : Trigger.new){
            carsDealer.UpdateCars(auto.Brand_Name__c);  
        }    
        
    } else if (Trigger.isUpdate){
        for(Cars__c auto: Trigger.New){
            carsDealer.UpdateCars(auto.Brand_Name__c);
            carsDealer.carsPrice('');
            carsDealer.carCount(auto.Cars_Shop_Name__c);
        }    
    } else if (Trigger.isDelete){
        for(Cars__c carShop: Trigger.Old){
            carsDealer.carCount(carShop.Cars_Shop_Name__c);
        }
    }   
}