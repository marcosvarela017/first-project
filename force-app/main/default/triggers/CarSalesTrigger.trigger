trigger CarSalesTrigger on Car_Sales_Motor_Shop__c (After Insert, After Update, After Delete) {
    
    if (Trigger.isInsert){
        
        String islandid;
        String carShopId;
        string color;
        string brandType;
        string allBrand;
        string shop;    
        integer numOfCars;
        string model;
        string cOrigin;
        
        for(Car_Sales_Motor_Shop__c carShop: Trigger.New){
            islandid= carShop.Island_Stand_Location__c;
            carShopId= carShop.Id;
            color= carsAttribute.rColors();
            brandType= carShop.Specific_Brand_Type__c;
            shop= carShop.Shop_Type__c; 
            numOfCars= carShop.Number_of_Cars_to_Create__c.intValue();
            
            carsDealer.updateLocation(carshop.Id, carshop.Island_Stand_Location__c);
            carsDealer.Models(model);
            
            If ( carshop.Shop_Type__c == 'Specific Brand' && carShop.Number_of_Cars_to_Create__c > 0){
                //carsDealer.carbrand(3, 0, 2021, brandType, island, carShopId);
                carsDealer.carbrand(numOfCars, 0, 2022, brandType, islandid, carShopId);
                carsDealer.carsValue(carshop.id);
            } else if (carshop.Shop_Type__c == 'All Brands' && carShop.Number_of_Cars_to_Create__c > 0){
                carsDealer.carbrand(numOfCars, 0, 2022, '', islandid, carShopId);
                carsDealer.carsValue(carshop.id);
            } else if(carShop.Number_of_Cars_to_Create__c == 0){
                carShop.addError('Cannot create a Shop with '+ carShop.Number_of_Cars_to_Create__c + ' cars.');  
            }
        }
        
        
    } else if (Trigger.isUpdate) {
        for(Car_Sales_Motor_Shop__c carShop: Trigger.New){
          //  carsDealer.carsValue(carshop.Id); 
        }    
    } else if (Trigger.isDelete) {
        for(Car_Sales_Motor_Shop__c carShop: Trigger.Old){
            if (carshop.Number_of_Cars_to_Create__c > 0){
                carShop.addError('Cannot delete Shop which related Cars. '+ 'Your shop has '+ carShop.Number_of_Cars_to_Create__c + ' cars.');
            }
        }    
    }
}